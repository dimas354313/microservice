from flask import Flask, request, jsonify, make_response
import pymysql

# Server Flask
app = Flask(__name__)

# Koneksi ke db_buku
mydb = pymysql.connect(
    host='127.0.0.1',
    user='root', 
    password='@B1s4411d',
    database = 'db_buku'
)

# Menentukan dan melakukan Routing index
@app.route('/')
@app.route('/index')
def index():
    return "Berhasil Terkoneksi"  #jika berhasil akan return kata tersebut -- Sudah Berhasil

# Methods Get
@app.route('/get_data_pelanggan', methods=['GET'])
def get_data_pelanggan():
	try:
		query1 = "SELECT * FROM tbl_pelanggan WHERE 1=1"
		values1 = ()

		id_pelanggan = request.args.get("id_pelanggan")
		nama_pelanggan = request.args.get("nama_pelanggan")
		alamat = request.args.get("alamat")

		if id_pelanggan:
			query1 += " AND id_pelanggan=%s "
			values1 += (id_pelanggan,)
		if nama_pelanggan:
			query1 += " AND nama_pelanggan LIKE %s " 
			values1 += ("%"+nama_pelanggan+"%", )
		if alamat:
			query1 += " AND alamat=%s "
			values1 += (alamat,)
	
		mycursor = mydb.cursor()
		mycursor.execute(query1, values1)	
		row_headers = [x[0] for x in mycursor.description]
		data = mycursor.fetchall()
		json_data = []
		for result in data:
			json_data.append(dict(zip(row_headers, result)))
		return make_response(jsonify(json_data),200)

	except Exception as e:
		return("Error: " + str(e))

@app.route('/get_data_buku', methods=['GET'])
def get_data_buku():
	try:
		query = "SELECT * FROM tbl_buku WHERE 1=1"
		values = ()

		id_buku = request.args.get("id_buku")
		judul = request.args.get("judul")
		isbn = request.args.get("isbn")

		if id_buku:
			query += " AND id_buku=%s "
			values += (id_buku,)
		if judul:
			query += " AND judul LIKE %s " 
			values += ("%"+judul+"%", )
		if isbn:
			query += " AND isbn=%s "
			values += (isbn,)

		mycursor = mydb.cursor()
		mycursor.execute(query, values)	
		row_headers = [x[0] for x in mycursor.description]
		data = mycursor.fetchall()
		json_data = []
		for result in data:
			json_data.append(dict(zip(row_headers, result)))
			print(type(json_data))
		return make_response(jsonify(json_data),200)

	except Exception as e:
		return("Error: " + str(e))


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)