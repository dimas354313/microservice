from flask import Flask, jsonify, request
import requests
import json

app = Flask(__name__)

@app.route('/server1/gettasks', methods=['GET'])
def gettasks():
	url = 'http://119.8.60.144:5000/get_data_pelanggan'
	response = requests.request('GET', url)
	# print(type(response))
	# print(type(response.text))
	# print(type(json.loads(response.text)))
	return jsonify(json.loads(response.text))

@app.route('/server1/gettasks', methods=['GET'])
def gettasks4():
	url = 'http://119.8.60.144:5000/get_data_buku'
	response = requests.request('GET', url)
	# print(type(response))
	# print(type(response.text))
	# print(type(json.loads(response.text)))
	return jsonify(json.loads(response.text))

@app.route('/server2/gettasks', methods=['GET'])
def gettasks1():
	url = 'http://94.74.127.32:3001/new'
	response = requests.request('GET', url)
	return response.json()

@app.route('/server2/gettasks1/<title>', methods=['GET'])
def gettasks2(title):
	page = request.args.get('page')
	if not page:
		url = 'http://94.74.127.32:3001/search/%s' % title
		response = requests.request('GET', url)
	else:
		#menerima inputan dengan placeholder
		#url = 'http://94.74.127.32:3001/search/%s/%s' % (title, page,)
		
		#menerima inputan dengan format
		url = f'http://94.74.127.32:3001/search/{title}/{page}'
		response = requests.request('GET', url)
	return response.json()

@app.route('/server2/gettasks2/<isbn>', methods=['GET'])
def gettasks3(isbn):
	url = 'http://94.74.127.32:3001/books/%s' % isbn
	response = requests.request('GET', url)
	return response.json()

if __name__ == '__main__':
	app.run(debug=True, port=5000, host='0.0.0.0')